# Migrating from geoip module to geoip2 module of nginx
install nginx and geoip2 module using ./nginx_install.sh script.

ubuntu
nginx 1.16.0


# GeoIP module from 
https://github.com/leev/ngx_http_geoip2_module

# Example config

##### nginx.conf #####

```
user www-data; 
worker_processes 2;
worker_rlimit_nofile 65536;
pid /var/run/nginx.pid;

events {
	worker_connections  4096;
        multi_accept on;
        accept_mutex off;
        use epoll;
}


http {

        ##
        # Basic Settings
        ##

        sendfile on;
        tcp_nopush on;
        tcp_nodelay on;
	server_tokens off;
	etag off;

	##
	#  Keepalives
	##
        keepalive_timeout 60s;
	keepalive_requests 10000;

	add_header X-Frame-Options "SAMEORIGIN";
        add_header X-XSS-Protection "1; mode=block";        
	add_header X-Content-Type-Options nosniff;

	types_hash_max_size 2048;
        client_max_body_size 8m;
	client_body_buffer_size 8m;
        client_body_timeout 60s;
        client_header_timeout 30s;
	fastcgi_read_timeout 60s;

        include mime.types;
        default_type application/octet-stream;

    	geoip2 /usr/share/GeoIP/GeoLite2-Country.mmdb {
        	auto_reload 60m;
        	$geoip2_data_country_code country iso_code;
    	}
 
 	##
	# GeoIP config
	##
    	map $geoip2_data_country_code $allowed_country {
        	default no;
        	US yes;
		SG yes;
    	}

    	##
	# IP address exclusions
	##
    	geo $exclusions {
        	default 0;
		192.168.0.0/24 1;
	}
...
}
```

##### vhost config ##### 

```
server {
        listen   80; ## listen for ipv4; this line is default and implied
        server_name example.com;

        root /var/www;
        index app.php;

	access_log /var/log/nginx/access.log;
        error_log /var/log/nginx/error.log;

	##
	# Cache static files
	##

	location ~* \.(?:manifest|appcache|xml)$ {
          expires -1;
	  log_not_found off;
        }

        # Feed
        location ~* \.(?:rss|atom)$ {
          expires 1h;
          add_header Cache-Control "public";
          access_log off;
          log_not_found off;
        }

        # Media: images, icons, video, audio, HTC
        location ~* \.(?:jpg|jpeg|gif|png|ico|cur|gz|svg|svgz|ogg|ogv|webm|htc|mp4)$ {
          expires 1M;
          access_log off;
          log_not_found off;
	  etag on;
          add_header Cache-Control "public";
        }

        # CSS and Javascript
        location ~* \.(?:css|js|woff|woff2)$ {
          expires 1y;
          access_log off;
	  log_not_found off;
          add_header Cache-Control "public";
        }

	# PROD
    	location ~ ^/app\.php(/|$) {

		# GeoIP blocking
                if ($allowed_country = yes) {
                        set $exclusions 1;
                }

                if ($exclusions = "0") {
                        return 403;
                }
		
        	fastcgi_pass 127.0.0.1:9000;
        	fastcgi_split_path_info ^(.+\.php)(/.*)$;
        	include fastcgi_params;
       		fastcgi_param SCRIPT_FILENAME $realpath_root$fastcgi_script_name;
       		fastcgi_param DOCUMENT_ROOT $realpath_root;
       		internal;
	}	

	# Deny request to .php other than app.php, app_dev.php
	location ~ \.php$ {
     		return 404;
   	}

        location / {

		## GeoIP blocking
                if ($allowed_country = yes) {
                        set $exclusion 1;
                }

                if ($exclusion = "0") {
                        return 403;
		}
	
		try_files $uri $uri/ /app.php$is_args$args;
        }
}
```


# Install

```
sudo su
./nginx_install.sh
```
