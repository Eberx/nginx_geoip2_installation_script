#!/bin/bash

set -e

##
#  install nginx latest and geoip2 support
##

NGINX="http://nginx.org/download/nginx-1.16.0.tar.gz"
NGINX_GEOIP2_MODULE="https://github.com/leev/ngx_http_geoip2_module.git"
GEOIP_DB="http://geolite.maxmind.com/download/geoip/database/GeoLite2-Country.mmdb.gz"

##
# check privileges
##
if ! [ $(id -u) = 0 ]; then
   echo "The script need to be run as root." >&2
   exit 1
fi

##
# Backup existing configuration and remove installed nginx
##

if [ -d /etc/nginx ]; then
	cp -R /etc/nginx ~/
fi
# check installed or not
pkgExist=$(dpkg -l | grep nginx | awk '{ print $2 }' | wc -l)
pkgList=$(dpkg -l | grep nginx | awk '{ print $2 }'| xargs)

if [ "$pkgExist" -gt 0 ]; then
	apt purge $pkgList
fi

##
# Download nginx, clone nginx geoip2 module
##
if [ ! -d /tmp/nginx ]; then
	mkdir -p /tmp/nginx
fi
wget $NGINX -O /tmp/nginx.tar.gz; tar -xf /tmp/nginx.tar.gz -C /tmp/nginx --strip-components=1
if [ -d /tmp/ngx_http_geoip2_module ]; then
	rm -rf /tmp/ngx_http_geoip2_module
fi
git clone $NGINX_GEOIP2_MODULE /tmp/ngx_http_geoip2_module

##
# Download Maxmind GeoIP2 module and unzip
##
wget $GEOIP_DB -O /usr/share/GeoIP/GeoLite2-Country.mmdb.gz && \
gunzip -f /usr/share/GeoIP/GeoLite2-Country.mmdb.gz

##
# install required libraries
##
LC_ALL=C.UTF-8 add-apt-repository ppa:maxmind/ppa -y
apt install build-essential libpcre3-dev zlib1g-dev libmaxminddb-dev libssl-dev -y

##
# Compile nginx with GeoIP2 module
# /usr/lib/nginx/modules/ngx_stream_geoip2_module.so
##

echo "Compiling nginx with GeoIP2 module"
cd /tmp/nginx && \
./configure --prefix=/etc/nginx \
--sbin-path=/usr/sbin/nginx \
--modules-path=/usr/lib/nginx/modules \
--conf-path=/etc/nginx/nginx.conf \
--error-log-path=/var/log/nginx/error.log \
--http-log-path=/var/log/nginx/access.log \
--pid-path=/var/run/nginx.pid \
--lock-path=/var/run/nginx.lock \
--http-client-body-temp-path=/var/cache/nginx/client_temp \
--http-proxy-temp-path=/var/cache/nginx/proxy_temp \
--http-fastcgi-temp-path=/var/cache/nginx/fastcgi_temp \
--http-uwsgi-temp-path=/var/cache/nginx/uwsgi_temp \
--http-scgi-temp-path=/var/cache/nginx/scgi_temp \
--user=www-data \
--group=www-data \
--with-compat \
--with-file-aio \
--with-threads \
--with-http_addition_module \
--with-http_auth_request_module \
--with-http_dav_module \
--with-http_flv_module \
--with-http_gunzip_module \
--with-http_gzip_static_module \
--with-http_mp4_module \
--with-http_random_index_module \
--with-http_realip_module \
--with-http_secure_link_module \
--with-http_slice_module \
--with-http_ssl_module \
--with-http_stub_status_module \
--with-http_sub_module \
--with-http_v2_module \
--with-mail \
--with-mail_ssl_module \
--with-stream \
--with-stream_realip_module \
--with-stream_ssl_module \
--with-stream_ssl_preread_module \
--add-dynamic-module=/tmp/ngx_http_geoip2_module \
--with-cc-opt='-g -O2 -fPIE -fstack-protector-strong -Wformat -Werror=format-security -Wp,-D_FORTIFY_SOURCE=2 -fPIC' --with-ld-opt='-Wl,-Bsymbolic-functions -fPIE -pie -Wl,-z,relro -Wl,-z,now -Wl,--as-needed -pie'
make
make install

##
# Create required folders
##
if [ ! -d /var/log/nginx ]; then
mkdir -p /var/log/nginx
fi
if [ ! -d /var/lib/nginx ]; then
mkdir -p /var/lib/nginx
fi
if [ ! -d /var/cache/nginx ]; then
mkdir -p /var/cache/nginx
fi

##
# Re-enable nginx configuration
##
if [ -d /etc/nginx ]; then
	rm -rf /etc/nginx/*
	cp -R ~/nginx/* /etc/nginx/
else
	echo "/etc/nginx folder does not exist after nginx installed."
	exit 1
fi


##
# Nginx systemctl configs
##
cat << EOF > /tmp/nginx.service
[Unit]
Description=A high performance web server and a reverse proxy server
After=network.target
 
[Service]
Type=forking
PIDFile=/var/run/nginx.pid
ExecStartPre=/usr/sbin/nginx -t -q -g 'daemon on; master_process on;'
ExecStart=/usr/sbin/nginx -g 'daemon on; master_process on;'
ExecReload=/usr/sbin/nginx -g 'daemon on; master_process on;' -s reload
ExecStop=-/sbin/start-stop-daemon --quiet --stop --retry QUIT/5 --pidfile /run/nginx.pid
TimeoutStopSec=5
KillMode=mixed
 
[Install]
WantedBy=multi-user.target
EOF
mv /tmp/nginx.service /etc/systemd/system/

##
# Enable nginx systemctl, start nginx
##
systemctl enable nginx && \
systemctl start nginx && \
systemctl status nginx
